import commonFiles
import matplotlib.pyplot as mp

read_values=commonFiles.fileRead('matches.csv',[10,1])
winner=read_values[0]
years=read_values[1]

teams=[] #number of teams participated
for i in winner:
    if i not in teams:
        if i!='': #excluding the matches where no body won
            teams.append(i)

print(winner)

#making each team as objects
class winningteams():
    def __init__(self, y):
        self.X = {}

    def get(self):
        return list(self.X.values())

    def gets(self):#gives out dictonary
        return self.X

    def alltheyears(self):#returns the list of years
        return list(self.X.keys())

    def seter(self, year):
        self.X[year]=self.X[year]+1

    def zero(self):
        for i in range(2008,2018):
            if i not in self.X:
                self.X[str(i)]=0

#making callable objects
listofobj = []
for i in range(0,len(teams)):
    listofobj.append(winningteams(i))
    listofobj[i].zero()


for i in range(0,len(winner)): #iterator for matches csv
    for j in range(0,len(teams)): #user data iterator
        if winner[i]==teams[j]: #longTeamList == teams
            listofobj[j].seter(years[i])

#Displaying segrigated data
for i in range(0,len(teams)):
    print(teams[i],"\n",listofobj[i].gets(),"\n")

#plotting
mp.subplot(1,1,1)
colours=["orange","purple","brown","r","g","b","yellow","black","grey","pink","m","c","olive","maroon"]
mp.bar(list(listofobj[0].gets().keys()),listofobj[0].get(),color=colours[0],width=0.8,bottom=0)
data=[0 for i in range(0,len(listofobj[1].get()))]
for i in range(1,len(teams)):
    for j in range(0,len(listofobj[i].get())):
        data[j]=data[j]+listofobj[i-1].get()[j]
    mp.bar(list(listofobj[i].gets().keys()),listofobj[i].get(),color=colours[i],width=0.8,bottom=data)

mp.xlabel("Years")
mp.ylabel("Matches Won")
mp.title("Matches Won of all the teams")
#mp.gca()
mp.legend(teams,bbox_to_anchor=(1.02, 1))
mp.grid()
mp.show()