import commonFiles
deliveries,matches,runs,balls=commonFiles.fileRead('deliveries.csv',[0,8,17]),commonFiles.csv2Dict('matches.csv',[0,1]),{},{}
for i in range(0,len(deliveries[2])): #iterator of deliveries
    if matches[deliveries[0][i]]=='2015': #iterator of user data
        runs[deliveries[1][i]],balls[deliveries[1][i]]=(runs[deliveries[1][i]]+int(deliveries[2][i]),balls[deliveries[1][i]]+1) if (deliveries[1][i] in runs) else (int(deliveries[2][i]),1)
for i in runs:
    runs[i]=(runs[i]*6)/balls[i]
commonFiles.plot(commonFiles.tup2dic(commonFiles.sort(runs,1,0,10)),"Players","Runs Per Over","Most Economic Bowlers of 2015") #plot