import commonFiles
print("Which stadium proved to be in the best setting for betting and getting runs")
deliveries,matches,output,output_matches=commonFiles.fileRead('deliveries.csv',[0,15]),commonFiles.csv2Dict('matches.csv',[0,15]),{},{}
for i in range(0,len(deliveries[1])):
    output[matches[str(deliveries[0][i])]]=output[matches[str(deliveries[0][i])]]+int(deliveries[1][i]) if matches[str(deliveries[0][i])] in output else int(deliveries[1][i])
for i in range(1,len(matches)+1):
        output_matches[matches[str(i)]]=1 if matches[str(i)] not in output_matches else output_matches[matches[str(i)]]+1
for i in output:
    output[i]=output[i]/output_matches[i]
commonFiles.plot(output,"Stadiums","Runs per Matches","Top scoring Stadiums per match")