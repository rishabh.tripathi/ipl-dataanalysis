import csv
import matplotlib.pyplot as mp
from collections import OrderedDict

def sort(file,by,slicefrom,sliceby):
    d=sorted(file.items(), key=lambda t: t[by])
    return d if (slicefrom==sliceby) else d[slicefrom:sliceby] 
    
def tup2dic(d):
    di={}
    for i in range(len(d)):
        di[d[i][0]]=d[i][1]
    return di

def fileRead(filename,columnToImport):
    with open(filename,'r') as csv_file:
        reader = csv.reader(csv_file)
        next(reader)
        importedColumns={}
        for i in range(0,len(columnToImport)):
            importedColumns[i]=[]
        for line in reader:
            for i in range(0,len(columnToImport)):
                importedColumns[i].append(line[columnToImport[i]])
    return importedColumns

def csv2Dict(filename,columnToImport):
    with open(filename,'r') as csv_file:
        reader = csv.reader(csv_file)
        next(reader)
        importedColumns={}
        for line in reader:
            importedColumns[line[columnToImport[0]]]=line[columnToImport[1]]
    return importedColumns

def plot(dictonary, labelx, labely, top_title):
    print(dictonary)
    mp.bar(dictonary.keys(),(dictonary.values()), label=dictonary.keys())
    mp.xlabel(labelx)
    mp.ylabel(labely)
    mp.title(top_title)
    mp.xticks(rotation=50)
    mp.grid()
    mp.show()

def plot1(dictonary):
    print(dictonary)
    keysList=[]
    valuesList=[]
    for i in dictonary:
        keysList.append(i[0])
        valuesList.append(i[1])

    valuesList.reverse()
    keysList.reverse()
    print(valuesList)
    print(keysList)
    mp.bar(keysList,valuesList)
    # mp.xlabel(labelx)
    # mp.ylabel(labely)
    # mp.title(top_title)
    mp.grid()
    mp.show()
#import time
#start_time = time.clock()
#print(time.clock() - start_time, "seconds")